var gulp = require('gulp');
var inject = require('gulp-inject');

gulp.task('default', ['inject']);

gulp.task('inject', function(done) {
  gulp.src('./index.html')
    .pipe(inject(gulp.src('./js/components/*.js', { read: false }), {
      name: 'components',
      relative: true
    }))
    .pipe(inject(gulp.src('./js/systems/*.js', { read: false }), {
      name: 'systems',
      relative: true
    }))
    .pipe(gulp.dest('./'))
    .on('end', done);
});