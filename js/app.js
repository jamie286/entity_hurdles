var af;
var lastTime;
var world;

function init () {
	world = new CES.World();
	world.addSystem(new app.s.Render());
	world.addSystem(new app.s.Physics());

	var player = new CES.Entity();
	player.addComponent(new app.c.Box(30, 30));
	player.addComponent(new app.c.Position(100, 400));
	player.addComponent(new app.c.Velocity(0, 0));
	player.addComponent(new app.c.Rotation(0));
	player.addComponent(new app.c.Colour('#ff00ff'));
	world.addEntity(player);

	// var floor = new CES.Entity();
	// floor.addComponent(new app.c.Position(0, 0));
	// floor.addComponent(new app.c.Static());
	// floor.addComponent(new app.c.Box(app.ctx.w, 10));
	// world.addEntity(floor);

	lastTime = 0;
	af = new window.AnimationFrame(60);
	af.request(update);
}

function update (time) {
	app.dt = time - lastTime;
	world.update(time);
	lastTime = time;
	af.request(update);
}