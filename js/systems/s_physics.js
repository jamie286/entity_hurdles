(function () {
	var physicsWorld = Physics();
	var bodies = {};

	// gravity
	physicsWorld.add(Physics.behavior('constant-acceleration', {
		acc: { x: 0, y: -0.0008 }
	}));

	// bounds
	var bounds = Physics.aabb(0, 0, app.ctx.w, app.ctx.h);
	physicsWorld.add(Physics.behavior('edge-collision-detection', {
		aabb: bounds,
		restitution: 0.3
	}));
	physicsWorld.add(Physics.behavior('body-impulse-response'));

	// collision detection
	physicsWorld.add(Physics.behavior('body-collision-detection'));
	physicsWorld.add(Physics.behavior('sweep-prune'));

	app.s.Physics = CES.System.extend({
		update: function (time) {
			physicsWorld.step(time);
			var entities = this.world.getEntities('position', 'velocity', 'rotation', 'box');
			entities.forEach(function (e) {
				var body = bodies[e.id];
				var c_pos = e.getComponent('position');
				c_pos.x = body.state.pos.x;
				c_pos.y = body.state.pos.y;
				var c_vel = e.getComponent('velocity');
				c_vel.x = body.state.vel.x;
				c_vel.y = body.state.vel.y;
				var c_rot = e.getComponent('rotation');
				c_rot.pos = body.state.angular.pos;
			});
		},
		addedToWorld: function (world) {
			world.entityAdded('position', 'box', 'static').add(function (e) {
				var c_pos = e.getComponent('position');
				var c_box = e.getComponent('box');
				var body = Physics.body('rectangle', {
					x: c_pos.x,
					y: c_pos.y,
					width: c_box.w,
					height: c_box.h,
					treatment: 'static',
					restitution: 0
				});
				bodies[e.id] = body;
				physicsWorld.addBody(body);
			});

			world.entityAdded('position', 'velocity', 'rotation', 'box').add(function(e) {
				var c_pos = e.getComponent('position');
				var c_vel = e.getComponent('velocity');
				var c_box = e.getComponent('box');
				var body = Physics.body('rectangle', {
					x: c_pos.x,
					y: c_pos.y,
					vx: c_vel.x,
					vy: c_vel.y,
					width: c_box.w,
					height: c_box.h,
					restitution: 1
				});
				bodies[e.id] = body;
				physicsWorld.addBody(body);
			});
			world.entityRemoved('position', 'velocity', 'rotation', 'box').add(function(e) {
				var body = bodies[e.id];
				physicsWorld.removeBody(body);
			});
			this._super(world);
		}
	});
})();