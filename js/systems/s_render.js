(function () {
	var shapes = {};
	var stage = new createjs.Stage('canvas');
	var bg = new createjs.Shape();
	bg.graphics.beginFill('#000000').drawRect(0, 0, app.ctx.w, app.ctx.h);
	stage.addChild(bg);
	
	app.s.Render = CES.System.extend({
		update: function () {
			var entities = this.world.getEntities('position', 'box');
			entities.forEach(function (e) {
				var shape = shapes[e.id];
				var c_box = e.getComponent('box');
				var c_pos = e.getComponent('position');
				shape.x = c_pos.x;
				shape.y = app.ctx.h - c_pos.y;

				var c_rot = e.getComponent('rotation');
				if (c_rot) shape.rotation = 180/Math.PI * c_rot.pos;
			});

			stage.update();
		},
		addedToWorld: function (world) {
			world.entityAdded('position', 'box').add(function(e) {
				var c_box = e.getComponent('box');
				var c_pos = e.getComponent('position');
				var shape = new createjs.Shape();
				var c_colour = e.getComponent('colour');
				var colString = c_colour? c_colour.col : '#ffffff';
				shape.graphics.beginFill(colString).drawRect(0, 0, c_box.w, c_box.h);
				shape.setBounds(0, 0, c_box.w, c_box.h);
				shape.regX = c_box.w/2;
				shape.regY = c_box.h/2;
				shape.x = c_pos.x;
				shape.y = app.ctx.h - c_pos.y;
				stage.addChild(shape);
				shapes[e.id] = shape;
			});
			world.entityRemoved('position', 'box').add(function(e) {
				var shape = shapes[e.id];
				stage.removeChild(shape);
			});
			this._super(world);
		}
	});
})();